# retropie-archive


## Images for SD/MMC 


### rpi3

````
 wget -c --no-check-certificate   "https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi2_rpi3.img.gz"
````

The tested, working image for RPI3 is at :
https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi2_rpi3.img.gz

### rpi4


The image needed for RPI4 is:
https://github.com/RetroPie/RetroPie-Setup/releases/download/4.7.1/retropie-buster-4.7.1-rpi4_400.img.gz
(apt-get has an issue.)


## Precompiled Packages 

e.g.

lr-ppsspp tar.gz https://files.retropie.org.uk/binaries/buster/rpi4/


https://files.retropie.org.uk/binaries/buster/rpi4/kms/libretrocores/lr-flycast.tar.gz 

https://files.retropie.org.uk/binaries/buster/rpi4/kms/libretrocores/lr-ppsspp.tar.gz 


https://files.retropie.org.uk/binaries/buster/rpi4/kms/emulators/mupen64plus.tar.gz



## Installing... 

````
= = = = = =
Installing dependencies for 'lr-flycast' : Dreamcast emulator - Reicast port for libretro
= = = = = = = = = = = = = = = = = = = = =


= = = = = = = = = = = = = = = = = = = = =
Installing 'lr-flycast' : Dreamcast emulator - Reicast port for libretro
= = = = = = = = = = = = = = = = = = = = =

Downloading https://files.retropie.org.uk/binaries/buster/rpi4/kms/libretrocores/lr-flycast.tar.gz.asc ...
2022-04-17 09:24:54 URL:https://files.retropie.org.uk/binaries/buster/rpi4/kms/libretrocores/lr-flycast.tar.gz.asc [870/870] -> "/tmp/tmp.Zdr3PvWD9p/lr-flycast.tar.gz.asc" [1]
Downloading https://files.retropie.org.uk/binaries/buster/rpi4/kms/libretrocores/lr-flycast.tar.gz ...
````











